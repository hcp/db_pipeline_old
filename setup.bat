@echo off
rem ---------------------------------------------------------------------------
rem Setup processing for
rem
rem Environment Variable Prequisites
rem
rem   JAVA_HOME       Directory of Java Version
rem
rem
rem $Id: setup.bat,v 1.1 2008/11/13 16:23:07 mohanar Exp $
rem ---------------------------------------------------------------------------


if "%1"=="" goto :usage
if "%2"=="" goto :usage

set PATH=%JAVA_HOME%\bin;%PATH%

:continue

echo SETIING UP PIPELINE UTILITIES

echo Using JAVA_HOME:           %JAVA_HOME%
echo .
echo Verify java version (with 'java -version')
call java -version

:: First set WORK_DIR to script folder, then MAVEN_HOME relative to that.
set WORK_DIR=%~dp0
set MAVEN_HOME=%WORK_DIR%\maven-1.0.2

:: If that exists, then show working folders and continue.
if exist %MAVEN_HOME% goto SHOW_DIRS

:: If not, try to do it relative with the pipeline folder.
set WORK_DIR=%~dp0\pipeline
set MAVEN_HOME=%WORK_DIR%\maven-1.0.2

if not exist %MAVEN_HOME% goto FOLDER_ERROR

:SHOW_DIRS

echo %WORK_DIR%
echo %MAVEN_HOME%

:EXECUTE

rem EXECUTE MAVEN Setup

%MAVEN_HOME%\bin\maven -d %WORK_DIR% "-Dadmin.email=%1" "-Dsmtp.server=%2" "-Dxnat.url=%3" pipeline:setup 

goto :END

:USAGE

echo Usage: setup.bat admin_email smtp_server xnat_url
goto :END

:FOLDER_ERROR

echo Couldn't find the Maven folder in either %~dp0 or %~dp0/pipeline.

:END

